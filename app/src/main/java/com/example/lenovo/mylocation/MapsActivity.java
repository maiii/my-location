package com.example.lenovo.mylocation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.mylocation.Model.AddressObject;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kcode.permissionslib.main.OnRequestPermissionsCallBack;
import com.kcode.permissionslib.main.PermissionCompat;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/*
implement more than one interface
 */
public class MapsActivity extends FragmentActivity  implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private GoogleMap mMap;

    LocationManager locationManager;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    int stu = 0;
    Location mLastLocation;
    Double movedlat, movedLng;
    String address;
    Place place;
    String descripion;

    List<Address> addressModels;
    Realm mRealm;


    @BindView(R.id.editText_address)
    TextView editText_address;

    @BindView(R.id.imageView_selectLocation)
    ImageView imageView_selectLocation;

    @BindView(R.id.editText_savedAddress)
    EditText editText_savedAddress;

    @BindView(R.id.button_addAddress)
    Button button_addAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        checkPermision();
/*
get current lang & lat then start new activity
if search by place name get the nearby place in onActivity result with request code 2
 */
        imageView_selectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {


                    LatLngBounds latLngBounds = new LatLngBounds(
                            new LatLng(movedlat, movedLng),
                            new LatLng(movedlat, movedLng));


                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setBoundsBias(latLngBounds)
                                    .build(MapsActivity.this);
                    startActivityForResult(intent, 2);


                } catch (Exception e) {
                    Intent intent =
                            null;
                    try {
                        intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(MapsActivity.this);
                    } catch (GooglePlayServicesRepairableException e1) {
                        e1.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e1) {
                        e1.printStackTrace();
                    }
                    startActivityForResult(intent, 2);

                }


            }
        });
        /*
        add address button action

         */
        button_addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (editText_savedAddress.getText().toString().equals("")) {

                    Toast.makeText(MapsActivity.this, "Please Add Place Name ", Toast.LENGTH_SHORT).show();
                    editText_savedAddress.requestFocus();
                } else {
                    addAddressToDB();
                    Intent intent = new Intent(MapsActivity.this, AddresslistActivity.class);
                    startActivity(intent);
                }


            }
        });
/*
edittext onclicklistner to make btn enable when begin to write after return with valid address
 */

        editText_savedAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                button_addAddress.setEnabled(true);
                button_addAddress.setTextColor(Color.parseColor("#ffffff"));
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        buildGoogleApiClient();
        // Add a marker in Sydney and move the camera
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }


        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {


                return false;
            }
        });

         // change lat and lang when change camera (map pin)
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(final CameraPosition cameraPosition) {

                movedlat = cameraPosition.target.latitude;
                movedLng = cameraPosition.target.longitude;


                // get user location address
                getCurrentAddress(movedlat, movedLng);

            }
        });

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                getCurrentAddress(movedlat, movedLng);
            }
        });
    }


    /*
    ask a permisssion to use location service.
     */

    private void checkPermision() {
        PermissionCompat.Builder builder = new PermissionCompat.Builder(MapsActivity.this);
        builder.addPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION});

        builder.addPermissionRationale(" ACCESS_FINE_LOCATION ");
        builder.addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
            @Override
            public void onGrant() {

                init();

            }

            @Override
            public void onDenied(String permission) {

                Toast.makeText(MapsActivity.this, "permision Not granted", Toast.LENGTH_SHORT).show();
            }
        });
        builder.build().request();

    }

    // call request location update
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //  mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

// also need to request location update
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    // to move to the current location
    // connect and move map to current location now
    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;


        if (location == null) {

        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            // enable get my Location Button On Map
            //  mMap.setMyLocationEnabled(true);
        }


        // open the map one  user location one time when activity start
        stu++;
        if (stu == 1) {

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 15));

        }

    }



    public void init() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);


        mRealm = Realm.getDefaultInstance();

        hideKeyboardFrom(this);
        // location manager to check gps if permission ok
        locationManager = (LocationManager) MapsActivity.this.getSystemService(Context.LOCATION_SERVICE);





        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
checkConnection();
        } else {
            showGPSDisabledAlertToUser();
        }



    }

    /*
    method to hide keyboard if appear when back
     */

    public static void hideKeyboardFrom(Activity activity) {

        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {


        }
    }

/*
method to appear dialoug if gps

 */
private void showGPSDisabledAlertToUser() {


    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


    // set title
    alertDialogBuilder.setTitle("Enable Location");

    // set dialog message
    alertDialogBuilder
            .setMessage("Your Location Setting is Set Off \n Enable Location To Use The App")
            .setCancelable(false)
            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int id) {
                    // go to location setting on mobile
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 55);

                }
            })
            .setNegativeButton("No",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int id) {
                    //disable wifi

                }
            });

    // create alert dialog
    AlertDialog alertDialog = alertDialogBuilder.create();

    // show it
    alertDialog.show();



}
/*
getCurrentAddress method take lang , lat then  set text on edittext
-param lat double take latitude
-param lang double take langitude
 */
public void getCurrentAddress(Double lat, Double lng) {
    // Get the location manager
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    if (locationManager != null) {

        try {

            if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }


        } catch (Exception ex) {
            Log.i("msg", "fail to request location update, ignore", ex);
        }
        if (locationManager != null) {
            mLastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        }
        Geocoder gcd = new Geocoder(getBaseContext(),
                Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(lat, lng, 1);

            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                editText_address.setText(address);


            }

        } catch (Exception e) {
            e.printStackTrace();
            //   Toast.makeText(this, " " + e, Toast.LENGTH_SHORT).show();
        }
    }
}

    // get address when back from google places api
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


// if choose place and back with data about the place then move with camera on this place
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {

                hideKeyboardFrom(this);
                place = PlaceAutocomplete.getPlace(this, data);

                Double placeLat = place.getLatLng().latitude;
                Double placeLng = place.getLatLng().longitude;


                descripion = String.valueOf(place.getAddress());

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(placeLat, placeLng), 15));

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e("Tag", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode==55)
        {

            checkConnection();
        }


    }
    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public void checkConnection(){
        if(isOnline()){
            Toast.makeText(MapsActivity.this, "You are connected to Internet", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(MapsActivity.this, "You are not connected to Internet", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     method to save address in database
    */
    private void addAddressToDB() {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {


                    try {


                        AddressObject add = new AddressObject();


                        add.setName(editText_savedAddress.getText().toString());
                        add.setDescrption(address);


                        realm.copyToRealm(add);


                    } catch (RealmPrimaryKeyConstraintException e) {
                        Toast.makeText(getApplicationContext(), "Primary Key exists, Press Update instead", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    }
