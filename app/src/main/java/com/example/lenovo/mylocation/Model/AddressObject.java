package com.example.lenovo.mylocation.Model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class AddressObject extends RealmObject {

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_AGE = "descrption";

    @Required  // must be enter to database
    private String name;
    private String descrption;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }





}

