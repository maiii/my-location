package com.example.lenovo.mylocation.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.mylocation.Model.AddressObject;
import com.example.lenovo.mylocation.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class AddressApdater extends RecyclerView.Adapter<AddressApdater.MyViewHolder>{





    private final Context context;

    RealmResults<AddressObject> results ;

    Realm realm ;
    TextView textView_nodata;


/*
constructor
-param results with all data get from realm when user save it
-paraam textview_nodta if no data return or save on realm show text with no addresses
 */

    public AddressApdater(Context context,   RealmResults<AddressObject> results , Realm realm ,TextView textView_nodata ) {
        this.context = context;
        this.results = results;
        this.realm= realm;
        this.textView_nodata= textView_nodata ;


    }


    // inflates the row layout from xml when needed
    @Override
    public AddressApdater.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MyViewHolder myViewHolder = new MyViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_address, viewGroup, false));
        return myViewHolder;

    }
    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull AddressApdater.MyViewHolder myViewHolder,final int i) {

        myViewHolder. textView_addressName.setText(""+results.get(i).getName());
        myViewHolder. textView_addressDescription.setText(""+results.get(i).getDescrption());



        myViewHolder.imageView_deleteRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        AddressObject addressObject = realm.where(AddressObject.class).equalTo(AddressObject.PROPERTY_NAME, results.get(i).getName()).findFirst();

                        if (addressObject != null) {
                            addressObject.deleteFromRealm();
                        }

                        if (results.isEmpty())
                            textView_nodata.setVisibility(View.VISIBLE);
                    }
                });





                notifyDataSetChanged();


            }
        });


    }
// total numbers of rows
    @Override
    public int getItemCount() {
        return  results.size();
    }


    // stores and recycles views as they are scrolled off screen

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.textView_addressName)
        TextView textView_addressName;


        @BindView(R.id.textView_addressDescription)
        TextView textView_addressDescription ;

        @BindView(R.id.imageView_deleteRow)
        ImageView imageView_deleteRow ;







        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

}
