package com.example.lenovo.mylocation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {
Thread thread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);
        init();
    }
/*
Method to sleep screen for 3 seconds then start the next activity
 */
    private void init() {


        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);


                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // After the splash screen duration, route to the right activities
                    Intent intent = new Intent(SplashScreenActivity.this, AddresslistActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        thread.start();


    }

}
