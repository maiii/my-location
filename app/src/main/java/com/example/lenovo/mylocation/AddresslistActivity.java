package com.example.lenovo.mylocation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lenovo.mylocation.Adapters.AddressApdater;
import com.example.lenovo.mylocation.Model.AddressObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class AddresslistActivity extends AppCompatActivity {

    @BindView(R.id.button_addAddress)
    Button button_addAddress ;

    @BindView(R.id.recylerView_Address)
    RecyclerView recylerView_Address ;

    @BindView(R.id.textView_noData)
    TextView textView_noData ;


    Realm mRealm;
    List<AddressObject> myAddress = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addresslist);
        ButterKnife.bind(this);
        mRealm = Realm.getDefaultInstance();


        readAddressList();



        button_addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddresslistActivity.this,MapsActivity.class);
                startActivity(intent);


            }
        });

    }
/*
method to make connection with realm and  read data from realm to show on recycleview
 */

    private void readAddressList() {


        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<AddressObject> results = realm.where(AddressObject.class).findAll();
                myAddress = realm.where(AddressObject.class).findAll();

               // check if list of data is empty then make no data text visible
                if (myAddress.isEmpty())
                {

                    textView_noData.setVisibility(View.VISIBLE);
                }
                else
                {
                    AddressApdater adapter = new AddressApdater(AddresslistActivity.this,results,mRealm,textView_noData);
                    recylerView_Address.setLayoutManager(new LinearLayoutManager(AddresslistActivity.this, LinearLayoutManager.VERTICAL, false));
                    recylerView_Address.setAdapter(adapter);

                }

            }
        });


    }
    }

